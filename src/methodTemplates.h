#ifndef METHODTEMPLATES_H
#define METHODTEMPLATES_H

#include "queryParser.h"

void methodTemplatesInit( const char *_methodDir );
void methodTemplatesCleanup();

char* getMethodsStr( char _delimiter );

/* Forward declaration */
typedef struct ldns_struct_pkt ldns_pkt;
int generateResponse( const query *_q, ldns_pkt *_pkt );

#endif // METHODTEMPLATES_H
