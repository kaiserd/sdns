#include <ldns/ldns.h>
#include <stdio.h>
#include <dirent.h>

#include "zonesign.h"

/* #include <limits.h> */
/* #define MAX_NAME_LEN (_POSIX_NAME_MAX + _POSIX_PATH_MAX) */
#define MAX_NAME_LEN 1024

static ldns_zone *g_zone = NULL;
static ldns_key_list *g_keys = NULL;

static int getKeyflagsFromZone( ldns_key *_key ) {
  size_t i;
  ldns_rr_list *rrlist;
  ldns_rr *rr;
  int flags = -1;
  int keytag = ldns_key_keytag( _key );

  assert( g_zone != NULL );

  rrlist = ldns_zone_rrs( g_zone );
  for( i = 0; i < ldns_rr_list_rr_count( rrlist ); ++i ) {
    rr = ldns_rr_list_rr( rrlist, i );
    if( ldns_rr_get_type(rr) == LDNS_RR_TYPE_DNSKEY ) {
      if( keytag == ldns_calc_keytag(rr) ) {
        flags = 256;
        break;
      }
      /* KSK has keytag + 1 */
      if( keytag + 1 == ldns_calc_keytag(rr) ) {
        flags = 257;
        break;
      }
    }
  }

  return flags;
}



static ldns_key_list *readKeys( const char *_path ) {
  DIR *dir;
  struct dirent *entry;

  ldns_status status;
  char keyfile_name[MAX_NAME_LEN];
  FILE *keyfile;
  ldns_key *key;
  ldns_key_list *keys;
  int line_nr;
  char *p;
  ldns_rdf *owner;

  assert( g_zone != NULL );

  keys = ldns_key_list_new();

  if( NULL != (dir = opendir( _path )) ) {
    while( NULL != (entry = readdir(dir)) ) {
      if( DT_REG & entry->d_type && 
          NULL != (p = strrchr( entry->d_name, '.' )) && 
          !strncmp( p, ".private", 9 ) ) {

        /* Read key */
        snprintf( keyfile_name, MAX_NAME_LEN, "%s/%s", _path, entry->d_name );
        keyfile = fopen( keyfile_name, "r" );
        line_nr = 0;
        if (!keyfile) {
          fprintf(stderr,
              "Error: unable to read %s: %s\n",
              keyfile_name,
              strerror(errno));
          continue;
        }
        else {
          status = ldns_key_new_frm_fp_l(&key, keyfile, &line_nr);
          fclose(keyfile);
          if (status != LDNS_STATUS_OK) {
            fprintf(stderr, "Error reading key at line %d: %s\n",
                line_nr, ldns_get_errorstr_by_id(status));
            continue;
          }
        }

        /* Set owner */
        owner = ldns_rdf_clone( ldns_rr_owner( ldns_zone_soa( g_zone ) ) );
        ldns_key_set_pubkey_owner( key, owner );

        ldns_key_list_push_key( keys, key );

        printf( "Read key %s\n", keyfile_name );
      }
    }
    closedir( dir );
  }
  else {
    perror( _path );
  }

  return keys;
}

ldns_rdf *zoneSignInit( const char *_path ) {
  char fname[MAX_NAME_LEN];
  FILE *fd;
  ldns_rdf *origin = NULL;
  ldns_rr *origin_soa = NULL;

  int linenr;
  uint32_t ttl = LDNS_DEFAULT_TTL;
  ldns_rr_class class = LDNS_RR_CLASS_IN;	
  ldns_status status;

  ldns_key_list *keys;
  ldns_key *key;
  int keyflags;
  int i;

  /* Read zonefile */
  snprintf( fname, MAX_NAME_LEN, "%s/zone.signed", _path);
  fd = fopen(fname, "r");
  if( !fd ) {
    fprintf( stderr, "Unable to read %s. Try unsigned...\n", fname );

    snprintf( fname, MAX_NAME_LEN, "%s/zone", _path);
    fd = fopen(fname, "r");
    if( !fd ) {
      fprintf( stderr, "Unable to read %s\n", fname );
      return NULL;
    }
  }

  status = ldns_zone_new_frm_fp_l(&g_zone, fd, origin, ttl, class, &linenr);
  fclose( fd );
  if( status != LDNS_STATUS_OK ) {
    fprintf( stderr, "Cant't read zonefile %s: Error %s at %d\n",
        fname, ldns_get_errorstr_by_id(status), linenr );
    return NULL;
  }

  /* Get origin */
  origin_soa = ldns_zone_soa(g_zone);
  if( !origin_soa ) {
    fprintf( stderr, "Cant't read zonefile %s: Missing SOA record", fname );
    ldns_zone_deep_free( g_zone );
    g_zone = NULL;
    return NULL;
  }

  /* Read keys and keep only ZSKs if more the one key exists */
  g_keys = ldns_key_list_new();
  keys = readKeys( _path );
  if( ldns_key_list_key_count(keys) > 0 ) {
    for( i = 0; i < ldns_key_list_key_count(keys); ++i ) {
      key = ldns_key_list_key(keys, i);
      keyflags = getKeyflagsFromZone(key);

      if( 257 == keyflags ) {
        printf( "Remove KSK(%d) from keylist\n", ldns_key_keytag(key)+1 );
        ldns_key_free( key );
      }
      else if( 256 == keyflags ){
        /* valid zsk key */
        ldns_key_list_push_key( g_keys, key );
      }
      else {
        fprintf( stderr, "No valid DNSKEY entry in zonefile. Probably a ZSK" );
        /* probably valid zsk key */
        ldns_key_list_push_key( g_keys, key );
      }
    }
  }
  /* TODO ldns_key_list_pop has a bug, and there is noc ldns_key_clone -> 
   * little memory leak here */
  /* ldns_key_list_free( keys ); */

  printf( "keyamount: %ld\n", ldns_key_list_key_count(g_keys) );


  return NULL;
}


ldns_rdf *zoneSignGetOrigin() {
  assert( g_zone != NULL );
  return ldns_rr_owner( ldns_zone_soa( g_zone ) );
}

int zoneSignResponse( ldns_rr *_question, ldns_pkt *_pkt ) {
  ldns_rr_list *rrlist;
  ldns_rr *rr;
  size_t i;
  int count_rr = 0;
  int count_rrsig = 0;

  if( !g_zone ) {
    /* Zone not initiliazed */
    return -1;
  }

  if( ldns_rr_get_type( _question ) == LDNS_RR_TYPE_SOA ) {
    rr = ldns_zone_soa( g_zone );
    ldns_pkt_safe_push_rr( _pkt, LDNS_SECTION_ANSWER, ldns_rr_clone(rr) );
    return 0;
  }

  /* TODO handle type any */

  /* Find records in zone file */
  rrlist = ldns_zone_rrs( g_zone );
  for( i = 0; i < ldns_rr_list_rr_count(rrlist); ++i ) {
    rr = ldns_rr_list_rr( rrlist, i );
    if( !ldns_rr_compare_no_rdata( rr, _question ) ) {
      ldns_pkt_safe_push_rr( _pkt, LDNS_SECTION_ANSWER, ldns_rr_clone(rr) );
      count_rr++;
    } else if( ldns_rr_get_type( rr ) == LDNS_RR_TYPE_CNAME &&
               ldns_rr_get_class(rr) == ldns_rr_get_class(_question) &&
               !ldns_dname_compare( ldns_rr_owner(rr), ldns_rr_owner(_question) ) ) {
      ldns_pkt_safe_push_rr( _pkt, LDNS_SECTION_ANSWER, ldns_rr_clone(rr) );
      /* TODO search and add rdate of cname to answer */
    } else if( ldns_pkt_edns_do( _pkt ) && ldns_rr_get_type( rr ) == LDNS_RR_TYPE_RRSIG ) {
      /* Find rrsig data */
      if( !ldns_dname_compare(ldns_rr_owner(rr), ldns_rr_owner(_question) ) &&
          ldns_rr_get_class(rr) == ldns_rr_get_class(_question) &&
          ldns_rdf2rr_type(ldns_rr_rrsig_typecovered(rr)) == ldns_rr_get_type(_question) ) {
        ldns_pkt_safe_push_rr( _pkt, LDNS_SECTION_ANSWER, ldns_rr_clone(rr) );
	count_rrsig++;
      }
    }
  }

  if(!count_rr && !count_rrsig) {
    return -1;
  }
  return count_rr - count_rrsig;
}

int zoneSignPacket( ldns_pkt *_pkt ) {
  ldns_rr_list *rrlist;
  ldns_rr_list *rrlistSigned;

  if( !g_keys || ldns_key_list_key_count(g_keys) < 1 ) {
    /* fprintf( stderr, "Can't sign RRs without keys loaded" ); */
    return -1;
  }

  /* No generic get section fct in library */
  ldns_rr_list *rrlists[] = {
    ldns_pkt_answer( _pkt ),
    ldns_pkt_authority( _pkt ),
    ldns_pkt_additional( _pkt )
  };
  ldns_pkt_section sections[] = {
    LDNS_SECTION_ANSWER,
    LDNS_SECTION_AUTHORITY,
    LDNS_SECTION_ADDITIONAL
  };

  for( int k = 0; k < 3; k++ ) {
    for( int i = ldns_rr_list_rr_count(rrlists[k]) - 1; i >= 0; i-- ) {
      // ldns_sign_public works per RRset,
      // so put every RR in own temporary list
      ldns_rr_list* rrlist = ldns_rr_list_new();
      ldns_rr_list_push_rr(rrlist, ldns_rr_list_rr(rrlists[k], i));
      rrlistSigned = ldns_sign_public( rrlist, g_keys );
      if( !ldns_pkt_safe_push_rr_list( _pkt, sections[k], rrlistSigned ) ) {
        fprintf( stderr, "Can't add RRSIGs to packet (%d)", LDNS_SECTION_ANSWER );
      }
    }
  }

  return 0;
}


void zoneGenerateNegativeResponse( ldns_pkt *_pkt ) {
  ldns_rdf* qowner;
  char next[256];
  ldns_rdf* next_rdf;
  ldns_rr* nsec;
  char* str;

  if( !g_zone ) {
    return;
  };

  qowner = ldns_rr_owner(ldns_rr_list_rr((ldns_pkt_question(_pkt)), 0));
  str = ldns_rdf2str(qowner);

  // append next. subdomain to question
  (void)snprintf(next, 256, "next.%s", str);
  next_rdf = ldns_rdf_new_frm_str(LDNS_RDF_TYPE_DNAME, next);

  // generate nsec record
  nsec = ldns_create_nsec(qowner, next_rdf, NULL);
  ldns_pkt_safe_push_rr(_pkt, LDNS_SECTION_AUTHORITY, nsec);

  // add zone soa
  ldns_pkt_safe_push_rr(_pkt, LDNS_SECTION_AUTHORITY, ldns_rr_clone(ldns_zone_soa(g_zone)));

  // declare as no error
  ldns_pkt_set_rcode( _pkt, LDNS_RCODE_NOERROR );

  // Cleanup
  free(str);
  ldns_rdf_free(next_rdf);

  return;
}


