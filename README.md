# Stateless DNS Echo Server

* Based on [Kaiser2014Stateless](https://netfuture.ch/2014/12/stateless-dns/)

# Build-/Rundependencies
* [ldns](https://www.nlnetlabs.nl/projects/ldns/)

# Usage
```
Usage:
    -h  This help
    -a  host4 (or host6) address
    -p  Port number
    -b  Base domain
    -m  Method directory
```

# TODO

*   Support inotify events to reload configs automatically
