#include <stdio.h>
#include <unistd.h>
#include <ldns/ldns.h>
#include <netinet/in.h>

#include "zonesign.h"
#include "statelessResponse.h"
#include "methodTemplates.h"
#include "queryParser.h"

static void usage();
static int connectUDP( ldns_rdf *_host, uint16_t _port );
static void eventLoop( int _udpSocket, int _verboseLevel );
static void cleanExit();


void usage()
{
  puts("Usage:");
  puts("    -h  This help");
  puts("    -a  host4 (or host6) address");
  puts("    -p  Portnumber");
  puts("    -b  Base domain");
  puts("    -m  Method directory");
  puts("    -z  Path to zonefiles (default: zones/default)");
  puts("    -v  Verbose (multiple possible)");
}

int connectUDP( ldns_rdf *_host, uint16_t _port ) {
  int udpSocket;
  struct sockaddr_storage *sas;
  size_t sasSize;
  struct timeval timeout;

  /* Generate socket addr (v4 or v6) */
  sas = ldns_rdf2native_sockaddr_storage( _host, _port, &sasSize);
  if(!sas) {
    fprintf(stderr, "Can't create socket address storage\n");
    return -1;
  }

  /* Connect and bind */
  int updSocket = ldns_udp_connect( sas, /*unused*/timeout );
  if( updSocket <= 0 ) {
    fprintf(stderr, "Can't connect to socket\n");
    free(sas);
    return -1;
  }
  if( bind( updSocket, (struct sockaddr*)sas, sasSize ) < 0 ) {
    fprintf(stderr, "Can't bind socket\n");
    close(updSocket);
    free(sas);
    return -1;
  }

  free(sas);
  return updSocket;
}

void eventLoop( int _udpSocket, int _verboseLevel ) {
  struct sockaddr_storage client;
  socklen_t clientSize;
  size_t msgLen;
  uint8_t *rawData;
  ldns_pkt *dnsPkt;
  ldns_status status;
  ldns_buffer *wireData;

  /* Allocate memory for output buffer */
  wireData = ldns_buffer_new(LDNS_MAX_PACKETLEN);
  if( wireData == NULL ) {
    fprintf( stderr, "buffer_new(): Can't allocate memory" );
    return;
  }

  while( true ) {
    /* Wait for new queries */
    clientSize = sizeof(client);
    rawData = ldns_udp_read_wire( _udpSocket, &msgLen, &client, &clientSize );
    if( rawData == NULL ) {
      /* fprintf( stderr, "upd_read_wire(): No data\n" ); */
      perror("udp_read_wire()");
      continue;
    }

    /* Convert raw data to ldns packet */
    status = ldns_wire2pkt( &dnsPkt, rawData, msgLen );
    if( status != LDNS_STATUS_OK ) {
      LDNS_FREE( rawData );
      fprintf( stderr, "wire2pkt(): Bad package - %s\n", ldns_get_errorstr_by_id( status ) );
      continue;
    }

    /* Construct answer (in same pkt) */
    (void) statelessResponse( dnsPkt, _verboseLevel );

    /* Send response */
    status = ldns_pkt2buffer_wire( wireData, dnsPkt );
    if( status != LDNS_STATUS_OK ) {
      ldns_pkt_free( dnsPkt );
      LDNS_FREE( rawData );
      fprintf( stderr, "pkt2buffer_wire(): %s\n", ldns_get_errorstr_by_id( status ) );
      break;
    }
    (void) ldns_udp_send_query( wireData, _udpSocket, &client, clientSize );
    ldns_buffer_clear( wireData );

    /* Cleanup */
    ldns_pkt_free( dnsPkt );
    LDNS_FREE( rawData );
  }

  /* Cleanup */
  ldns_buffer_free( wireData );
}

int main(
    int _argc,
    char **_argv)
{
  char *endptr;
  ldns_rdf *host = NULL;
  char *hostip;
  uint16_t port = 0;
  long int tmpPort = 0;
  const char *base = NULL;
  const char *methodPath = NULL;
  const char *zonePath = NULL;
  int option;
  int verboseLevel = 0;

  /* Parse command arguments */
  while( ( option = getopt(_argc, _argv, "ha:p:b:m:z:v" ) ) != -1 ) {
    switch(option) {
      case 'h': // Help
        usage();
        exit( EXIT_SUCCESS );
      case 'a': // Address (ipv4 or ipv6)
        host = ldns_rdf_new_frm_str( LDNS_RDF_TYPE_A, optarg );
        if( !host ) {
          host = ldns_rdf_new_frm_str( LDNS_RDF_TYPE_AAAA, optarg );
          if( !host ) {
            fprintf( stderr, "valid ip4 or ip6 address required (given %s)\n", optarg );
            usage();
            exit( EXIT_FAILURE );
          }
        }
        hostip = strdup(optarg);
        break;
      case 'p': // Port
        tmpPort = strtol( optarg, &endptr, 10 );
        if( *endptr == '\0' ) {
          if( tmpPort > 0 && tmpPort < 65536 ) {
            port = tmpPort;
          }
          else {
            fprintf( stderr, "Invalid port number: %ld\n", tmpPort );
            usage();
            exit( EXIT_FAILURE );
          }
        }
        else {
          fprintf( stderr, "Conversion error: %s?\n", endptr );
          usage();
          exit( EXIT_FAILURE );
        }
        break;
      case 'b': // Base
        base = optarg;
        break;
      case 'm': // Method path
        methodPath = optarg;
        break;
      case 'z':
        zonePath = optarg;
        break;
      case 'v': // Verbose output
        verboseLevel++;
        break;
      default:
        usage();
        exit( EXIT_FAILURE );
    }
  }

  /* Default values */
  if( !host ) {
    hostip = strdup("127.0.0.2");
    host = ldns_rdf_new_frm_str( LDNS_RDF_TYPE_A, "127.0.0.2" );
  }
  if( !port ) {
    port = 53;
  }
  if( !base ) {
    base = "echo.localhost";
  }
  if( !methodPath ) {
    methodPath = "methods";
  }
  if( !zonePath ) {
    zonePath = "zones/default";
  }

  /* Initialize methods and parser */
  zoneSignInit( zonePath );
  methodTemplatesInit( methodPath );
  printf( "Supported methods: %s\n", getMethodsStr( ',' ) );
  queryParserInit( base, hostip );
  printf( "Base: %s\n", base );
  (void)atexit( cleanExit ); // TODO handle sigint to call exit

  /* Connect upd addr:port */
  int udpSocket = connectUDP( host, port );
  if(udpSocket <= 0) {
    ldns_rdf_free( host ); //no longer needed
    free(hostip);
    exit( EXIT_FAILURE );
  }
  printf( "Start listening at " );
  ldns_rdf_print( stdout, host );
  printf( ":%d\n", port );
  ldns_rdf_free( host ); //no longer needed
  free(hostip);


  /* Start eventloop */
  fflush(stdout);
  eventLoop( udpSocket, verboseLevel );


  return EXIT_SUCCESS;
}

void cleanExit() {
  puts( "clean" );
  /* Cleanup */
  queryParserCleanup();
  methodTemplatesCleanup();
}
