#ifndef STATELESSREPONSE_H
#define STATELESSREPONSE_H

#include "methodTemplates.h"

/* Generate stateless answer in same dnsPkt */
int statelessResponse( ldns_pkt *_dnsPkt, int _verboseLevel );

#endif //STATELESSREPONSE_H
