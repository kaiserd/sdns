CC=gcc
CFLAGS= -g -O0 -I src $(OPTFLAGS)
# LIBS=-lldns $(OPTLIBS)
LDFLAGS=-Wl,-rpath=$(shell cd ../../ldns/lib/ && pwd) -lldns $(OPTLIBS)

SOURCES=$(wildcard src/*.c src/**/*.c)
OBJECTS=$(patsubst src/%.c, build/%.o, $(SOURCES))

TARGET=statelessDNS

all: $(TARGET)

$(TARGET): build $(OBJECTS) Makefile
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJECTS) -o $@

sudo: $(TARGET)
	sudo /sbin/setcap 'cap_net_bind_service=+ep' $(PWD)/$(TARGET)

build/%.o: src/%.c
	$(CC) $(CFLAGS) -c $< -o $@

build:
	mkdir -p build

clean:
	rm $(TARGET)
	rm $(OBJECTS)
	rm -rf build

.PHONY: all
