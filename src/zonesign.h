#ifndef ZONESIGN_H
#define ZONESIGN_H

/* Loads the zone.sign file and all *.private keys from *path* */
/* Returns origin if succed */
ldns_rdf *zoneSignInit( const char *_path );


ldns_rdf *zoneSignGetOrigin();


/* Answer requests with zonefile records */
int zoneSignResponse( ldns_rr *_question, ldns_pkt *_pkt );

/* Sign RRs in packets */
int zoneSignPacket( ldns_pkt *_pkt );

void zoneGenerateNegativeResponse( ldns_pkt *_pkt );



#endif
